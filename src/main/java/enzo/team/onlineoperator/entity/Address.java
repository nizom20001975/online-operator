package enzo.team.onlineoperator.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String nameUz;
    @Column(nullable = false)
    private String nameRu;
    @Column(nullable = false)
    private String nameEn;

    @Column(nullable = false)
    private String locationUz;
    @Column(nullable = false)
    private String locationRu;
    @Column(nullable = false)
    private String locationEn;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String descriptionUz;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String descriptionRu;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String descriptionEn;
}
