package enzo.team.onlineoperator.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer courseId;

    @Column(nullable = false)
    private String courseNameUz;
    @Column(nullable = false)
    private String courseNameRu;
    @Column(nullable = false)
    private String courseNameEn;

    @Column(nullable = false)
    private String courseTeacher;
    @Column(nullable = false)
    private String courseTeacherRu;

    @Column(nullable = false)
    private Integer coursePrice;

    @Column
    private Boolean isCourseActive;

    @Column(nullable = false,columnDefinition = "TEXT")
    private String courseDescriptionUz;
    @Column(nullable = false,columnDefinition = "TEXT")
    private String courseDescriptionRu;
    @Column(nullable = false,columnDefinition = "TEXT")
    private String courseDescriptionEn;

}
